import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

/* uruchomienie aplikacji */
WebUI.openBrowser(GlobalVariable.url)

/* wpisanie nazwy użytkownika */
WebUI.setText(findTestObject('Object Repository/DEMOBANK/PageLogin/Input_login'), user_login)

/* uruchomienie opcji dalej */
WebUI.click(findTestObject('Object Repository/DEMOBANK/PageLogin/Button_next'))

/* wpisanie hasła użytkownika */
WebUI.setText(findTestObject('Object Repository/DEMOBANK/PageLogin/Input_password'), user_password)

/* uruchomienie opcji Zaloguj sie */
WebUI.click(findTestObject('Object Repository/DEMOBANK/PageLogin/Button_login'))