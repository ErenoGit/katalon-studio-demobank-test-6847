<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Zestaw testów</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>110c59cd-2a0f-45d8-8d5b-a9c32b5aa83f</testSuiteGuid>
   <testCaseLink>
      <guid>a03ef701-e66f-4754-bbf4-e0652ea6b48d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DEMOBANK/Setup/Logowanie do aplikacji</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>76004b4b-3859-4388-8d7e-575b8f8693c4</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>2fa8be04-ef65-4a6b-9f46-14fcf7bc1540</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>48175d8b-b8b5-4e7b-8937-5ff82c7e391b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DEMOBANK/USERNAME/Sprawdzenie nazwy użytkownika</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3f62a9e0-84eb-463f-88d5-ab17cbbee3c5</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>454cb8c8-c3c2-4ebf-a6e7-9071e2e0d6c1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/DEMOBANK/PAYMENT/Wykonanie szybkiego przelewu</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
