import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys


/* Pętla na wszystkie dane testowe do przelewu z Data Files "Dane do przelewu" */
TestData daneDoPrzelewu = findTestData('Data Files/Dane do przelewu');
int rows = daneDoPrzelewu.getRowNumbers()
for (int i = 1; i <= rows; i++)
{
	/* uruchomienie opcji "Szybki przelew" */
	WebUI.click(findTestObject('Object Repository/DEMOBANK/PageDesktop/fast_payment'))
	
	/* Wybranie z listy adresów do kogo ma zostac wyslany przelew */
	WebUI.selectOptionByLabel(findTestObject('Object Repository/DEMOBANK/PageFastPayment/select_receiver'), 'Chuck Demobankowy', false)
	
	/* wpisanie kwoty przelewu  */
	WebUI.setText(findTestObject('Object Repository/DEMOBANK/PageFastPayment/input_amount'), daneDoPrzelewu.getObjectValue('Kwota', i))
	
	/* wpisanie tytułu przelewu  */
	WebUI.setText(findTestObject('Object Repository/DEMOBANK/PageFastPayment/input_title'), daneDoPrzelewu.getObjectValue('Tytuł', i))
	
	/* uruchomienie opcji "wykonaj" */
	WebUI.click(findTestObject('Object Repository/DEMOBANK/PageFastPayment/button_makePayment'))
	
	/* wyciągnięcie komunikatu */
	String message = WebUI.getText(findTestObject('Object Repository/DEMOBANK/PageDesktop/message'))
	
	/* sprawdzenie czy komunikat jest OK */
	assert message.contains('Przelew wykonany!')
}



/* Zamykanie przeglądarki */
WebUI.closeBrowser()